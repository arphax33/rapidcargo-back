package fr.cyclonic.rapidcargo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class RapidCargoBackApplication {
	public static void main(String[] args) {
		SpringApplication.run(RapidCargoBackApplication.class, args);
	}
}
