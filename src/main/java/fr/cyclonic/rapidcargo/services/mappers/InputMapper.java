package fr.cyclonic.rapidcargo.services.mappers;

import fr.cyclonic.rapidcargo.domain.entities.WarehouseMovement;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.InputDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface InputMapper {
    InputDto toDto(WarehouseMovement warehouseMovement);
    List<InputDto> toDtos(List<WarehouseMovement> warehouseMovements);

    @Mapping(target = "document", ignore = true)
    WarehouseMovement toEntity(InputDto inputDto);
}

