package fr.cyclonic.rapidcargo.services.mappers;

import fr.cyclonic.rapidcargo.domain.entities.WarehouseMovement;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.OutputDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OutputMapper {
    OutputDto toDto(WarehouseMovement warehouseMovement);
    List<OutputDto> toDtos(List<WarehouseMovement> warehouseMovements);

    WarehouseMovement toEntity(OutputDto outputDto);
}

