package fr.cyclonic.rapidcargo.services.mappers;

import fr.cyclonic.rapidcargo.domain.entities.WarehouseMovement;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.WarehouseMovementDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WarehouseMovementMapper {
    WarehouseMovementDto toDto(WarehouseMovement warehouseMovement);
    List<WarehouseMovementDto> toDtos(List<WarehouseMovement> warehouseMovements);
}

