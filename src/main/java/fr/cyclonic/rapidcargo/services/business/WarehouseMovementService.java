package fr.cyclonic.rapidcargo.services.business;

import fr.cyclonic.rapidcargo.domain.entities.WarehouseMovement;
import fr.cyclonic.rapidcargo.domain.enums.GoodType;
import fr.cyclonic.rapidcargo.domain.enums.WarehouseMovementType;
import fr.cyclonic.rapidcargo.domain.repositories.WarehouseMovementRepository;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.InputDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.OutputDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.WarehouseMovementDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.good.GoodDto;
import fr.cyclonic.rapidcargo.services.exceptions.BadRequestException;
import fr.cyclonic.rapidcargo.services.mappers.InputMapper;
import fr.cyclonic.rapidcargo.services.mappers.OutputMapper;
import fr.cyclonic.rapidcargo.services.mappers.WarehouseMovementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class WarehouseMovementService {
    private static final int AWB_REFERENCE_LENGTH = 11;

    @Autowired
    WarehouseMovementRepository warehouseMovementRepository;

    @Autowired
    WarehouseMovementMapper warehouseMovementMapper;

    @Autowired
    InputMapper inputMapper;

    @Autowired
    OutputMapper outputMapper;

    public Page<WarehouseMovementDto> getAllWarehouseMovements(Pageable pageable) {
        Page<WarehouseMovement> warehouseMovements = warehouseMovementRepository.findAll(pageable);

        return new PageImpl<>(new ArrayList<>(warehouseMovementMapper
                .toDtos(warehouseMovements.getContent())), pageable, warehouseMovements.getTotalElements());
    }

    public Page<OutputDto> getAllOutputs(Pageable pageable) {
        Page<WarehouseMovement> warehouseMovements = warehouseMovementRepository.findAll(WarehouseMovementType.OUTPUT, pageable);

        return new PageImpl<>(new ArrayList<>(outputMapper
                .toDtos(warehouseMovements.getContent())), pageable, warehouseMovements.getTotalElements());
    }

    public Page<InputDto> getAllInputs(Pageable pageable) {
        Page<WarehouseMovement> warehouseMovements = warehouseMovementRepository.findAll(WarehouseMovementType.INPUT, pageable);

        return new PageImpl<>(new ArrayList<>(inputMapper
                .toDtos(warehouseMovements.getContent())), pageable, warehouseMovements.getTotalElements());
    }

    public OutputDto createOutput(OutputDto outputDto) {
        verifyGoods(outputDto);

        outputDto.setMessageTime(LocalDateTime.now());
        outputDto.setMovementTime(LocalDateTime.now());

        return outputMapper.toDto(warehouseMovementRepository.save(outputMapper.toEntity(outputDto)));
    }

    public InputDto createInput(InputDto inputDto) {
        verifyGoods(inputDto);

        inputDto.setMessageTime(LocalDateTime.now());
        inputDto.setMovementTime(LocalDateTime.now());

        return inputMapper.toDto(warehouseMovementRepository.save(inputMapper.toEntity(inputDto)));
    }

    public void verifyCorrespondingInput(String code) {
        WarehouseMovement warehouseMovement = warehouseMovementRepository
                .findByGoodReference(WarehouseMovementType.INPUT, code);

        if (warehouseMovement == null) {
            throw new BadRequestException("WAREHOUSE_MOVEMENT.FORM.ERROR.NO_CORRESPONDING_GOOD");
        }
    }

    void verifyGoods(WarehouseMovementDto warehouseMovementDto) throws BadRequestException {
        warehouseMovementDto.getGoods().forEach(good -> {
            verifyReferenceLength(good);
            verifyAmountAndWeight(good);

            if (warehouseMovementDto.getType().equals(WarehouseMovementType.OUTPUT)) {
                verifyCorrespondingInput(good.getReference().getCode());
            }
        });
    }

    void verifyAmountAndWeight(GoodDto good) {
        if (good.getTotalRefAmount().getQuantity() < good.getAmount().getQuantity()
                || good.getTotalRefAmount().getWeight() < good.getAmount().getWeight()) {
            throw new BadRequestException("The total quantity and weight must be higher or equal than the quantity and weight");
        }
    }

    void verifyReferenceLength(GoodDto good) {
        if (good.getReference().getType().equals(GoodType.AWB)
                && good.getReference().getCode().length() != AWB_REFERENCE_LENGTH) {
            throw new BadRequestException("The reference code must be exactly 11 in length");
        }
    }
}
