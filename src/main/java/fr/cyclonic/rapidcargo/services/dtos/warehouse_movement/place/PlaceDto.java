package fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.place;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlaceDto {
    private String code;
    private String label;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null || getClass() != o.getClass()) return false;
        PlaceDto place = (PlaceDto) o;
        return code == place.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
