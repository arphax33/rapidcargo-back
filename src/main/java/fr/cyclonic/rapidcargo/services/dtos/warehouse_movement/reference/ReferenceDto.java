package fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.reference;

import fr.cyclonic.rapidcargo.domain.enums.GoodType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReferenceDto {
    private String code;
    private GoodType type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null || getClass() != o.getClass()) return false;
        ReferenceDto reference = (ReferenceDto) o;
        return code == reference.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
