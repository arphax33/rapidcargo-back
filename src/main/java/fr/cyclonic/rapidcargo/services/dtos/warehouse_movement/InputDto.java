package fr.cyclonic.rapidcargo.services.dtos.warehouse_movement;

import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.place.PlaceDto;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

@Data
@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class InputDto extends WarehouseMovementDto {
}
