package fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.good;

import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.amount.AmountDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.reference.ReferenceDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GoodDto {
    private ReferenceDto reference;
    private AmountDto amount;
    private AmountDto totalRefAmount;
    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null || getClass() != o.getClass()) return false;
        GoodDto place = (GoodDto) o;
        return Objects.equals(reference.getCode(), place.reference.getCode())
                && amount.getQuantity() == place.amount.getQuantity()
                && amount.getWeight() == place.amount.getWeight();
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference.getCode(), amount.getQuantity(), amount.getWeight());
    }
}
