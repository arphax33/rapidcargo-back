package fr.cyclonic.rapidcargo.services.dtos.warehouse_movement;

import fr.cyclonic.rapidcargo.domain.enums.WarehouseMovementType;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.good.GoodDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.place.PlaceDto;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Data
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class WarehouseMovementDto {

    private String id;
    private WarehouseMovementType type;
    private PlaceDto from;
    private PlaceDto to;
    private List<GoodDto> goods;
    private String customStatus;
    private PlaceDto declaredIn;
    private LocalDateTime movementTime;
    private LocalDateTime messageTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null || getClass() != o.getClass()) return false;
        WarehouseMovementDto warehouseMovement = (WarehouseMovementDto) o;
        return Objects.equals(id, warehouseMovement.id)
                && type == warehouseMovement.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }
}
