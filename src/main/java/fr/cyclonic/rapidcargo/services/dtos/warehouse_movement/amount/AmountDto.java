package fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.amount;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AmountDto {
    private int quantity;
    private int weight;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null || getClass() != o.getClass()) return false;
        AmountDto amount = (AmountDto) o;
        return quantity == amount.quantity
                && weight == amount.weight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, weight);
    }
}
