package fr.cyclonic.rapidcargo.services.dtos.warehouse_movement;

import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.document_ref.DocumentRefDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.place.PlaceDto;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

@Data
@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class OutputDto extends WarehouseMovementDto {
    private DocumentRefDto document;
}
