package fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.document_ref;

import fr.cyclonic.rapidcargo.domain.enums.DocumentType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentRefDto {
    private DocumentType type;
    private String reference;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null || getClass() != o.getClass()) return false;
        DocumentRefDto documentRef = (DocumentRefDto) o;
        return Objects.equals(reference, documentRef.reference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference);
    }
}
