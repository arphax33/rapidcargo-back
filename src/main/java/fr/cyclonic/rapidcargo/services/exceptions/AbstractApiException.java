package fr.cyclonic.rapidcargo.services.exceptions;

import org.springframework.http.HttpStatus;

import java.io.Serial;

public class AbstractApiException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;
    private final HttpStatus  errorStatus;

    AbstractApiException(HttpStatus errorStatus) {
        super();
        this.errorStatus = errorStatus;
    }

    AbstractApiException(String message, HttpStatus errorStatus) {
        super(message);
        this.errorStatus = errorStatus;
    }

    AbstractApiException(Throwable cause, HttpStatus errorStatus) {
        super(cause);
        this.errorStatus = errorStatus;
    }

    AbstractApiException(String message, Throwable cause, HttpStatus errorStatus) {
        super(message, cause);
        this.errorStatus = errorStatus;
    }

}
