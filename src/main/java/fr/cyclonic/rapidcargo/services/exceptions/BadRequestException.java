package fr.cyclonic.rapidcargo.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends AbstractApiException {

    private static final long      serialVersionUID = 1L;
    public static final HttpStatus httpStatus       = HttpStatus.BAD_REQUEST;

    public BadRequestException(String message) {
        super(message, httpStatus);
    }

    public BadRequestException(Throwable cause) {
        super(cause, httpStatus);
    }
}
