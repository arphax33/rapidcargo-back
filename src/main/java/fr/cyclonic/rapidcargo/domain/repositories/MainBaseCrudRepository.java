package fr.cyclonic.rapidcargo.domain.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface MainBaseCrudRepository<T> extends MongoRepository<T, String> {
    public long count();
}