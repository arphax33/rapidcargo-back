package fr.cyclonic.rapidcargo.domain.repositories;

import fr.cyclonic.rapidcargo.domain.entities.WarehouseMovement;
import fr.cyclonic.rapidcargo.domain.enums.WarehouseMovementType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;

public interface WarehouseMovementRepository extends MainBaseCrudRepository<WarehouseMovement> {
    @Query(value="{type:'?0'}")
    Page<WarehouseMovement> findAll(WarehouseMovementType type, Pageable pageable);

    @Query(value = "{type:'?0', 'goods.reference.code': '?1'}", fields = "{'goods.reference.code': 1}")
    WarehouseMovement findByGoodReference(WarehouseMovementType type, String reference);

}