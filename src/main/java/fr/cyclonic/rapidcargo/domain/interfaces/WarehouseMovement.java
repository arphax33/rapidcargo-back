package fr.cyclonic.rapidcargo.domain.interfaces;

import fr.cyclonic.rapidcargo.domain.enums.WarehouseMovementType;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.good.GoodDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.place.PlaceDto;

import java.time.Instant;

public interface WarehouseMovement {
    int id = 0;
    WarehouseMovementType type = null;
    PlaceDto from = null;
    PlaceDto to = null;
    GoodDto[] goods = null;
    String customStatus = null;
    Instant movedAt = null;
}
