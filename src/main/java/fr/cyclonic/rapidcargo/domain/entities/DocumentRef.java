package fr.cyclonic.rapidcargo.domain.entities;

import fr.cyclonic.rapidcargo.domain.enums.DocumentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class DocumentRef {
    private DocumentType type;
    private String reference;
}

