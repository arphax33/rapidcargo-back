package fr.cyclonic.rapidcargo.domain.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Amount {
    private int quantity;
    private int weight;
}

