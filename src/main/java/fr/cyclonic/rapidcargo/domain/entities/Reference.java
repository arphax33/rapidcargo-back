package fr.cyclonic.rapidcargo.domain.entities;

import fr.cyclonic.rapidcargo.domain.enums.GoodType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Reference {
    private String code;
    private GoodType type;
}

