package fr.cyclonic.rapidcargo.domain.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Good {
    private Reference reference;
    private Amount amount;
    private Amount totalRefAmount;
    private String description;
}

