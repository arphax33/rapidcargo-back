package fr.cyclonic.rapidcargo.domain.entities;

import fr.cyclonic.rapidcargo.domain.enums.WarehouseMovementType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Document("WarehouseMovement")
public class WarehouseMovement {
    @Id
    private String id;
    private WarehouseMovementType type;
    private Place from;
    private Place to;
    private Place declaredIn;
    private Good[] goods;
    private String customStatus;
    private DocumentRef document;
    private LocalDateTime movementTime;
    @CreatedDate
    private LocalDateTime messageTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null || getClass() != o.getClass()) return false;
        WarehouseMovement warehouseMovement = (WarehouseMovement) o;
        return id == warehouseMovement.id
                && type == warehouseMovement.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }
}