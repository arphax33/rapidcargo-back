package fr.cyclonic.rapidcargo.domain.enums;

import lombok.Getter;

@Getter
public enum DocumentType {
    T1("T1"),
    FOO("FOO"),
    BAR("BAR");

    private final String value;

    private DocumentType(String value) {
        this.value = value;
    }

}
