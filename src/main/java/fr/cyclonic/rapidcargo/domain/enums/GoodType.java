package fr.cyclonic.rapidcargo.domain.enums;

import lombok.Getter;

@Getter
public enum GoodType {
    AWB("AWB"),
    FOO("FOO"),
    BAR("BAR");

    private final String value;

    private GoodType(String value) {
        this.value = value;
    }

}
