package fr.cyclonic.rapidcargo.domain.enums;

import lombok.Getter;

@Getter
public enum WarehouseMovementType {
    OUTPUT("OUTPUT"),
    INPUT("INPUT");

    private final String value;

    private WarehouseMovementType(String value) {
        this.value = value;
    }

}
