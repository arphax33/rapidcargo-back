package fr.cyclonic.rapidcargo.configuration;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;

@Configuration
public class OpenAPIConfig {

	@Value("${url.base-url}")
	private String url;

	@Bean
	public OpenAPI myOpenAPI() {
		Server server = new Server();
		server.setUrl(url);
		server.setDescription("Server URL environment");

		Info info = new Info()
				.title("RapidCargo API")
				.version("0.0.1-SNAPSHOT");

		return new OpenAPI().info(info).servers(List.of(server));
	}
}