package fr.cyclonic.rapidcargo.controllers;

import fr.cyclonic.rapidcargo.domain.enums.WarehouseMovementType;
import fr.cyclonic.rapidcargo.services.business.WarehouseMovementService;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.InputDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.OutputDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.WarehouseMovementDto;
import fr.cyclonic.rapidcargo.services.exceptions.BadRequestException;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/warehouse_movements")
public class WarehouseMovementController {
    private static final Logger logger = LoggerFactory.getLogger(WarehouseMovementController.class);

    @Autowired
    WarehouseMovementService warehouseMovementService;

    @GetMapping(value = "", produces = "application/json")
    @Operation(summary = "Get All WAREHOUSE MOVEMENT")
    public ResponseEntity<Iterable<WarehouseMovementDto>> getAllWarehouseMovements(
            @RequestParam(name = "page") Integer page,
            @RequestParam(name = "size") Integer size,
            @RequestParam(required = false, name = "type") WarehouseMovementType type
    ) {
        if (logger.isInfoEnabled()) {
            logger.info(String.format("WAREHOUSE MOVEMENT CRUD, GET, api/warehouse_movements, Get all Warehouse Movements of type %s", type));
        }

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "movedAt");

        return ResponseEntity.ok(warehouseMovementService.getAllWarehouseMovements(pageable));

/*        return switch (type) {
            case INPUT -> ResponseEntity.ok(warehouseMovementService.getAllInputs(pageable));
            case OUTPUT -> ResponseEntity.ok(warehouseMovementService.getAllOutputs(pageable));
            case null -> ResponseEntity.ok(warehouseMovementService.getAllWarehouseMovements(pageable));
        };*/
    }

    @PostMapping(value = "/outputs")
    public ResponseEntity<OutputDto> createOutput(@RequestBody OutputDto outputDto){
        if (logger.isInfoEnabled()) {
            logger.info("WAREHOUSE MOVEMENT CRUD, POST, api/warehouse_movements/outputs, Create Warehouse Movement of type OUTPUT");
        }

        if (outputDto == null) { throw new BadRequestException("Request body is null"); }
        return ResponseEntity.ok(warehouseMovementService.createOutput(outputDto));
    }

    @GetMapping(value = "/outputs/verify")
    public ResponseEntity<Void> verifyCorrespondingInput(@RequestParam String goodReference){
        if (logger.isInfoEnabled()) {
            logger.info("WAREHOUSE MOVEMENT CRUD, POST, api/warehouse_movements/outputs/verify, Verify corresponding INPUT with good reference");
        }

        warehouseMovementService.verifyCorrespondingInput(goodReference);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/inputs")
    public ResponseEntity<InputDto> createInput(@RequestBody InputDto inputDto){
        if (logger.isInfoEnabled()) {
            logger.info("WAREHOUSE MOVEMENT CRUD, POST, api/warehouse_movements/inputs, Create Warehouse Movement of type INPUT");
        }

        if (inputDto == null) { throw new BadRequestException("Request body is null"); }
        return  ResponseEntity.ok(warehouseMovementService.createInput(inputDto));
    }
}
