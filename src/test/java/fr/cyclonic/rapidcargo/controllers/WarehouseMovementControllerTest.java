package fr.cyclonic.rapidcargo.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import fr.cyclonic.rapidcargo.RapidCargoBackApplicationTests;
import fr.cyclonic.rapidcargo.domain.entities.WarehouseMovement;
import fr.cyclonic.rapidcargo.domain.repositories.WarehouseMovementRepository;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.InputDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.OutputDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.WarehouseMovementDto;
import fr.cyclonic.rapidcargo.services.exceptions.BadRequestException;
import fr.cyclonic.rapidcargo.services.mappers.WarehouseMovementMapper;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import fr.cyclonic.rapidcargo.services.business.WarehouseMovementService;

import java.util.List;

class WarehouseMovementControllerTest extends RapidCargoBackApplicationTests {


    @InjectMocks
    WarehouseMovementController controller;

    @Mock
    WarehouseMovementService service;

    @Mock
    WarehouseMovementRepository repository;

    @Mock
    WarehouseMovementMapper mapper;

    @Mock
    OutputDto outputDto;

    @Mock
    InputDto inputDto;

    @Mock
    Pageable pageable;

    @Test
    void getAllWarehouseMovements_success() {
        // Arrange
        List<WarehouseMovement> movements = List.of(new WarehouseMovement());
        when(repository.findAll(pageable))
                .thenReturn(new PageImpl<>(movements));
        when(mapper.toDtos(movements))
                .thenReturn(List.of(new WarehouseMovementDto()));

        // Return Page instead of List
        when(service.getAllWarehouseMovements(pageable))
                .thenReturn(new PageImpl<>(List.of(new WarehouseMovementDto())));

        // Act
        ResponseEntity<Iterable<WarehouseMovementDto>> response =
                controller.getAllWarehouseMovements(0, 10, null);

        // Assert
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
    }

    @Test
    void getAllWarehouseMovements_emptyResult() {
        // Arrange
        List<WarehouseMovement> movements = List.of(new WarehouseMovement());
        when(repository.findAll(pageable))
                .thenReturn(new PageImpl<>(movements));
        when(mapper.toDtos(movements))
                .thenReturn(List.of(new WarehouseMovementDto()));

        // Return Page instead of List
        when(service.getAllWarehouseMovements(pageable))
                .thenReturn(new PageImpl<>(List.of(new WarehouseMovementDto())));

        // Act
        ResponseEntity<Iterable<WarehouseMovementDto>> response =
                controller.getAllWarehouseMovements(0, 10, null);

        // Assert
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
    }

    @Test
    void createOutput_success() {
        // Arrange
        when(service.createOutput(outputDto))
                .thenReturn(outputDto);

        // Act
        ResponseEntity<OutputDto> response = controller.createOutput(outputDto);

        // Assert
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertEquals(outputDto, response.getBody());
    }

    @Test
    void createOutput_nullDto() {
        // Act + Assert
        assertThrows(BadRequestException.class, () -> {
            controller.createOutput(null);
        });
    }

    @Test
    void createInput_success() {
        // Arrange
        when(service.createInput(inputDto))
                .thenReturn(inputDto);

        // Act
        ResponseEntity<InputDto> response = controller.createInput(inputDto);

        // Assert
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertEquals(inputDto, response.getBody());
    }

    @Test
    void createInput_nullDto() {
        // Act + Assert
        assertThrows(BadRequestException.class, () -> {
            controller.createInput(null);
        });
    }

    @Test
    void verifyCorrespondingInput_success() {
        // Act
        ResponseEntity<Void> response = controller.verifyCorrespondingInput("REF123");

        // Assert
        assertEquals(HttpStatusCode.valueOf(204), response.getStatusCode());
    }
}