package fr.cyclonic.rapidcargo.services.business;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import fr.cyclonic.rapidcargo.RapidCargoBackApplicationTests;
import fr.cyclonic.rapidcargo.domain.enums.GoodType;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.WarehouseMovementDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.amount.AmountDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.good.GoodDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.reference.ReferenceDto;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import fr.cyclonic.rapidcargo.domain.entities.WarehouseMovement;
import fr.cyclonic.rapidcargo.domain.enums.WarehouseMovementType;
import fr.cyclonic.rapidcargo.domain.repositories.WarehouseMovementRepository;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.InputDto;
import fr.cyclonic.rapidcargo.services.dtos.warehouse_movement.OutputDto;
import fr.cyclonic.rapidcargo.services.exceptions.BadRequestException;
import fr.cyclonic.rapidcargo.services.mappers.InputMapper;
import fr.cyclonic.rapidcargo.services.mappers.OutputMapper;
import fr.cyclonic.rapidcargo.services.mappers.WarehouseMovementMapper;

class WarehouseMovementServiceTest extends RapidCargoBackApplicationTests {

    @InjectMocks
    WarehouseMovementService service;

    @Mock
    WarehouseMovementRepository repository;

    @Mock
    WarehouseMovementMapper mapper;

    @Mock
    InputMapper inputMapper;

    @Mock
    OutputMapper outputMapper;

    @Mock
    Pageable pageable;

    InputDto inputDto;

    OutputDto outputDto;

    @Test
    void getAllWarehouseMovements_success() {
        // Arrange
        List<WarehouseMovement> movements = List.of(new WarehouseMovement());
        when(repository.findAll(pageable))
                .thenReturn(new PageImpl<>(movements));
        when(mapper.toDtos(movements)).thenReturn(List.of(new WarehouseMovementDto()));

        // Act
        Page<WarehouseMovementDto> result = service.getAllWarehouseMovements(pageable);

        // Assert
        assertFalse(result.isEmpty());
        verify(repository).findAll(pageable);
    }

    @Test
    void createOutput_success() {
        // Arrange
        outputDto = new OutputDto();
        outputDto.setGoods(new ArrayList<>());
        outputDto.setGoods(new ArrayList<>());
        when(outputMapper.toDto(any())).thenReturn(outputDto);
        when(outputMapper.toEntity(outputDto)).thenReturn(new WarehouseMovement());
        when(repository.save(any())).thenReturn(new WarehouseMovement());

        // Act
        OutputDto result = service.createOutput(outputDto);

        // Assert
        assertNotNull(result);
        verify(repository).save(any());
    }

    @Test
    void verifyCorrespondingInput_notFound() {
        // Arrange
        when(repository.findByGoodReference(WarehouseMovementType.INPUT, "REF123"))
                .thenReturn(null);

        // Act + Assert
        assertThrows(BadRequestException.class, () -> {
            service.verifyCorrespondingInput("REF123");
        });
    }

    @Test
    void getAllInputs_success() {
        // Arrange
        List<WarehouseMovement> movements = List.of(new WarehouseMovement());
        when(repository.findAll(WarehouseMovementType.INPUT, pageable))
                .thenReturn(new PageImpl<>(movements));
        when(inputMapper.toDtos(movements)).thenReturn(List.of(new InputDto()));

        // Act
        Page<InputDto> result = service.getAllInputs(pageable);

        // Assert
        assertFalse(result.isEmpty());
        verify(repository).findAll(WarehouseMovementType.INPUT, pageable);
    }

    @Test
    void getAllOutputs_success() {

        // Arrange
        List<WarehouseMovement> movements = List.of(new WarehouseMovement());
        when(repository.findAll(WarehouseMovementType.OUTPUT, pageable))
                .thenReturn(new PageImpl<>(movements));
        when(outputMapper.toDtos(movements)).thenReturn(List.of(new OutputDto()));

        // Act
        Page<OutputDto> result = service.getAllOutputs(pageable);

        // Assert
        assertFalse(result.isEmpty());
        verify(repository).findAll(WarehouseMovementType.OUTPUT, pageable);
    }

    @Test
    void createInput_success() {

        // Arrange
        inputDto = new InputDto();
        inputDto.setGoods(new ArrayList<>());
        when(inputMapper.toDto(any())).thenReturn(inputDto);
        when(inputMapper.toEntity(inputDto)).thenReturn(new WarehouseMovement());
        when(repository.save(any())).thenReturn(new WarehouseMovement());

        // Act
        InputDto result = service.createInput(inputDto);

        // Assert
        assertNotNull(result);
        verify(repository).save(any());
    }

    @Test
    void verifyReferenceLength_invalid() {
        // Arrange
        GoodDto goodDto = new GoodDto();
        goodDto.setReference(new ReferenceDto());
        goodDto.getReference().setCode("ABCD");
        goodDto.getReference().setType(GoodType.AWB);

        // Act + Assert
        assertThrows(BadRequestException.class, () -> {
            service.verifyReferenceLength(goodDto);
        });
    }

    @Test
    void verifyAmount_invalid() {
        // Arrange
        GoodDto goodDto = new GoodDto();
        goodDto.setAmount(new AmountDto());
        goodDto.setTotalRefAmount(new AmountDto());
        goodDto.getAmount().setQuantity(10);
        goodDto.getTotalRefAmount().setQuantity(3);

        // Act + Assert
        assertThrows(BadRequestException.class, () -> {
            service.verifyAmountAndWeight(goodDto);
        });
    }

    @Test
    void verifyWeight_invalid() {
        // Arrange
        GoodDto goodDto = new GoodDto();
        goodDto.setAmount(new AmountDto());
        goodDto.setTotalRefAmount(new AmountDto());
        goodDto.getAmount().setWeight(10);
        goodDto.getTotalRefAmount().setWeight(3);

        // Act + Assert
        assertThrows(BadRequestException.class, () -> {
            service.verifyAmountAndWeight(goodDto);
        });
    }
}